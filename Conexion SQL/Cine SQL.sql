create database cine
use cine

CREATE TABLE peliculas(
    id bigint identity(1,1) primary key,
    titulo VARCHAR(255) NOT NULL,
    anio SMALLINT NOT NULL
);

select * from peliculas