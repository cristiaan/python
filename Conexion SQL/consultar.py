from bd import conexion

try:
    with conexion.cursor() as cursor:
        cursor.execute("SELECT id, titulo, anio FROM peliculas")
        peliculas = cursor.fetchall()
        for i in peliculas:
            print(i)
except Exception as e:
    print("Ocurrio algun error")
finally:
    conexion.close()