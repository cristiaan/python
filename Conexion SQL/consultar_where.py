from bd import conexion
try:
    with conexion.cursor() as cursor:
        consulta = "SELECT id, titulo, anio FROM peliculas WHERE anio > ?"
        cursor.execute(consulta, (2000))

        peliculas = cursor.fetchall()

        for i in peliculas:
            print(i)
except Exception as e:
    print("Ocurrio algun error")
finally:
    conexion.close()