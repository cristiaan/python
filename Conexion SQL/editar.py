from bd import conexion

try:
    with conexion.cursor() as cursor:
        consulta = "UPDATE peliculas SET titulo = ? WHERE id = ?"
        nuevo = "El Zorro"
        ident = 4
        cursor.execute(consulta, (nuevo, ident))
    conexion.commit()
except Exception as e:
    print("Ocurrio algun error")
finally:
    conexion.close()