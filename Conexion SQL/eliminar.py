from bd import conexion

try:
    with conexion.cursor() as cursor:
        consulta = "DELETE FROM peliculas WHERE anio < ?"
        estreno = 2000
        cursor.execute(consulta, (estreno))
    conexion.commit()
except Exception as e:
    print("Ocurrio algun error")
finally:
    conexion.close()