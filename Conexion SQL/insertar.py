from bd import conexion

try:
    with conexion.cursor() as cursor:
        consulta = "INSERT INTO peliculas (titulo, anio) VALUES (?, ?)"
        cursor.execute(consulta, ("Volver al futuro 1", 1980))
        cursor.execute(consulta, ("El senor de los Anillos", 1994))
        cursor.execute(consulta, ("It", 2016))
except Exception as e:
    print("Ocurrio algun error")
finally:
    conexion.close()