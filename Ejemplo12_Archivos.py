from io import open

##Escribir dentro de un archivo
file = open("archivo.txt", "w")
mensaje = "Hoy es viernes\nYa es Fin de Semana\nEs 29 de Julio del 2022"
mensaje2 = "\nFeliz Fin de Semana"
file.write(mensaje)
file.write(mensaje2)
file.close()

print("\nMostrar Todo:")
#Leer un archivo
file = open("archivo.txt", "r")
contenido = file.read()
file.close()
print(contenido)

print("\nMostrar en formato Lista:")
#Leer y guardar linea por linea con LISTAS
file = open("archivo.txt", "r")
li = file.readlines()
file.close()
print(li)

#Aumentar una fila al archivo
print("\nAumentar al final del archivo:")
file = open("archivo.txt", "a")
file.write("\n!!!!!!!!!!!!!!!!!")
file.close()

#*********Punteros de un archivo***********
print("\nMostrar archvio con Punteros:")
file = open("archivo.txt", "r")
i=len(file.read())
print("i ==== ",i)

print("\n---mostrar hasta el 6---")
print(file.read(6))

print("\n---mostrar desde el 6--")
print(file.read())

print("\n---Volver a mostrar desde el incio---")
file.seek(0)
print(file.read())

print("\n---mostrar desde la mitad---")
file.seek(0)
i=len(file.read())/2

file.seek(i)
print(file.read())
file.close()

#Abrir un archivo en Lectura y Escritura
print("\nAbrir un archivo en Lectura y Escritura")
file = open("archivo.txt", "r+")
lista = file.readlines()
lista[1]=" Fila Recien Agregada \n"
file.seek(0)
file.writelines(lista)
file.seek(0)
print(file.read())
file.close()
