#Instalar Paquete
# pip install pyinstaller

#Crear el Ejecutable
# pyinstaller Interfaces2Calculadora.py

#Crear el Ejecutable SIN Terminal de Fondo
# pyinstaller --windowed Interfaces2Calculadora.py

#Crear el Ejecutable SIN Terminal de Fondo y SIN archivos extra
# pyinstaller --windowed --onefile Interfaces2Calculadora.py