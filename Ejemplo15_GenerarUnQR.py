#Generar un QR de una direccion

#Impotar Libreria
import qrcode
#Direccion de la cual generaremos el QR
data="https://www.umsa.bo/"
#Nombre de la Imagen QR que se va a generar
QRCodefile = "QRCode.png"
#Generamos el Codigo QR
QRimage=qrcode.make(data)
#Guardamos la imagen generada
QRimage.save(QRCodefile)