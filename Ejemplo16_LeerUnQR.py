#Importar Biblioteca
import cv2
#Nombre del archivo de la imagen QR
filename="QRCode.png"
#Leer el QR
image=cv2.imread(filename)
#Inicializar el detector QR
detector=cv2.QRCodeDetector()
#Detectar y Decodicar
data, vertices_array, binary_qrcode = detector.detectAndDecode(image)
#Verificar si es un codigo QR
if vertices_array is not None:
    print("QRCode:")
    print(data)
else:
    print("No es un QR")