#Excepciones

def divide():
    try:
        dat1=float(input("Leer dat1: "))
        dat2=float(input("Leer dat2: "))
        print("La divicion es: ", (dat1/dat2))
    except ValueError:
        print("El valor ingresado es incorrecto")
    except ZeroDivisionError:
        print("No se puede dividir entre 0")
    finally:
        print("Calculo Finalizado")

divide()