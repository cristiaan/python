#Excepciones

def comprobarEdad(edad):
    if edad<0:
        raise ZeroDivisionError ("Error de Edad menor a 0")

    if edad<20:
        return "Adolecente"
    if edad<40:
        return "Joven"
    if edad<65:
        return "Mayor"
    if edad<100:
        return "Maduro"

print(comprobarEdad(-88))