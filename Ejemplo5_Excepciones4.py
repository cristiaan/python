import math

def calcularRaiz(num1):
    if num1<0:
        raise ValueError("El numero no debe ser negativo")
    else:
        return math.sqrt(num1)

numero=(int(input("Leer numero: ")))
try:
    print(calcularRaiz(numero))
except ValueError as NoSePermitenNegativos:
    print("Error de numero negativo")

print("Fin del programa")