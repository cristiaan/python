from tkinter import Tk, Label, Button

class ventana:
    def __init__(self, master):
        self.master = master
        master.title("Mi Primera Interfaz")

        self.etiqueta = Label(master, text="Esta es una ventana")
        self.etiqueta.pack()

        self.botonSaludo = Button(master, text="Saludar", command=self.saludar)
        self.botonSaludo.pack()

        self.botonCerrar = Button(master, text="cerrar", command=master.quit)
        self.botonCerrar.pack()

    def saludar(self):
        print("hola")

root = Tk()
Miventana = ventana(root)
root.mainloop()